import os


class ProductionConfig:
    DEBUG = False
    TESTING = False
    APP_ENVIRONMENT = "production"
    _SQLALCHEMY_DATABASE_URI = None
    FEATURE_FLAGS_URL = "https://gitlab.com/api/v4/feature_flags/unleash/31433980"
    FEATURE_FLAGS_INSTANCE_ID = "gWEs8xuY2p8VczagcxmE"
    SENTRY_DSN = (
        "https://6c5921549fd0471fb95ff9d25f88d98c@o1075474.ingest.sentry.io/6080682"
    )

    @property
    def SQLALCHEMY_DATABASE_URI(self):
        return "postgresql://app:[OD3BuHg]%(csM%WMzWBjIRlqS$KQB:\
            }lRr@talk-booking-prod.cmkvdmjondnd.af-south-1.rds.amazonaws.com:5432/app"

    # def SQLALCHEMY_DATABASE_URI(self):
    #     if self._SQLALCHEMY_DATABASE_URI is None:
    #         self._SQLALCHEMY_DATABASE_URI = boto3.client(
    #             service_name="secretsmanager"
    #         ).get_secret_value(
    #             SecretId=f"db-connection-string-{self.APP_ENVIRONMENT}-new"
    #         )[
    #             "SecretString"
    #         ]

    #     return self._SQLALCHEMY_DATABASE_URI


class StagingConfig(ProductionConfig):
    DEBUG = True
    APP_ENVIRONMENT = "staging"
    _SQLALCHEMY_DATABASE_URI = None

    @property
    def SQLALCHEMY_DATABASE_URI(self):
        return "postgresql://app:xCJa2_0inPvPFl8o!(DC$GZ34]k:\
            F?jlKZr@talk-booking-stag.cmkvdmjondnd.af-south-1.rds.amazonaws.com:5432/app"


class TestConfig(ProductionConfig):
    DEBUG = True
    TESTING = True
    APP_ENVIRONMENT = "local"
    _SQLALCHEMY_DATABASE_URI = None

    @property
    def SQLALCHEMY_DATABASE_URI(self):
        return "postgresql://app:talkbooking@postgres:5432/talkbookingtest"


class LocalTestConfig(ProductionConfig):
    DEBUG = True
    APP_ENVIRONMENT = "local"
    _SQLALCHEMY_DATABASE_URI = None

    @property
    def SQLALCHEMY_DATABASE_URI(self):
        return "postgresql://postgres:talkbooking@localhost:5432/talkbooking_test"


class LocalConfig(ProductionConfig):
    DEBUG = True
    APP_ENVIRONMENT = "local"
    _SQLALCHEMY_DATABASE_URI = None

    @property
    def SQLALCHEMY_DATABASE_URI(self):
        return "postgresql://postgres:talkbooking@localhost:5432/talkbooking"


CONFIGS = {
    "production": ProductionConfig,
    "staging": StagingConfig,
    "test": TestConfig,
    "local_test": LocalTestConfig,
}


def load_config():
    """
    Load config based on environment
    :return:
    """

    return CONFIGS.get(os.getenv("APP_ENVIRONMENT"), LocalConfig)()
